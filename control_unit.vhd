----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 28.01.2019 14:33:42
-- Design Name: 
-- Module Name: control_unit - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values


-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity control_unit is
    Port (
        ------ Clock ,input active-low reset; output active high reset --------
        I_clk         : in std_logic;
        I_reset       : in std_logic;
        -----------------------------------------------------------------------
        
        -------- User signals-------------------------------------------------- 
        I_ackInput         : in std_logic;
        I_nextReady         : in std_logic;
        I_endInput          : in std_logic;
        O_ready             : out std_logic;
        O_layerIdle             : out std_logic;
        O_requestInput      : out std_logic
        -----------------------------------------------------------------------

     );
end control_unit;

architecture Behavioral of control_unit is

type t_state is(ST_INIT, ST_FIRST, ST_REQUEST_INPUT,ST_WAIT_NEXT);

signal state_current    : t_state;
signal state_future     : t_state;

signal requestInput      : std_logic;
signal ready             : std_logic;
signal layerIdle  : std_logic;
signal nextReady_comb  : std_logic;

signal nextReady_reg    :std_logic;
signal reset_next_reg     : std_logic;

begin
   
    sync_proc : process(I_clk,I_reset)
    begin
        if I_reset ='1' then
            state_current <= ST_INIT;
        elsif rising_edge(I_clk) then
            state_current<= state_future;
        end if;
    end process;
    
    comb_proc: process(state_current,nextReady_reg,I_endInput,I_ackInput)
    begin
        
        requestInput<='0';
        ready<='0';
        layerIdle<='0';
        reset_next_reg<='0';
                    
        case state_current is    
        ----------------------------------------------------------------------------
        ----------------------------------------------------------------------------
            when ST_INIT =>
                state_future<= ST_FIRST;  
        ----------------------------------------------------------------------------
        ----------------------------------------------------------------------------
        
            when ST_FIRST =>
                requestInput<='1';
                layerIdle<='1';          
            
                if I_ackInput ='1' then
                    state_future<= ST_REQUEST_INPUT;
                else
                    state_future<= ST_FIRST;
                end if;
        ----------------------------------------------------------------------------
        ---------------------------------------------------------------------------    
            when ST_REQUEST_INPUT =>
                if I_endInput ='1' then
                     requestInput<='0';
                     state_future<= ST_WAIT_NEXT;
                     ready<='1';
                else
                    requestInput<='1';
                     ready<='0';
                     state_future<= ST_REQUEST_INPUT;
                end if;
        ----------------------------------------------------------------------------
        ----------------------------------------------------------------------------           
            when ST_WAIT_NEXT =>
                
                if nextReady_reg ='1' then
                     reset_next_reg<='1';
                     state_future<= ST_REQUEST_INPUT;
                else
                     reset_next_reg<='0';
                     state_future<= ST_WAIT_NEXT;
                end if;
        ----------------------------------------------------------------------------
        ----------------------------------------------------------------------------            
            when others =>
                 state_future<=ST_INIT;
        end case;    
    end process;
   
   
    nextReady_comb<= '1' when (I_nextReady='1' or nextReady_reg<='1') and reset_next_reg='0' else '0';
   
    next_reg: process(I_clk,I_reset)
    begin
        
        if I_reset='1' then
            nextReady_reg<='0';
        elsif rising_edge(I_clk) then
            nextReady_reg<=nextReady_comb;
        end if;   
    end process;
    

    --------------OUTPUT signals------------------
    O_requestInput<=requestInput;
    O_ready<=ready;
    O_layerIdle<=layerIdle;
    
    
end Behavioral;