library ieee;   
use IEEE.STD_LOGIC_1164.ALL;
USE IEEE.NUMERIC_STD.ALL;
USE work.utils_pkg.ALL;
 
entity ArgMax is
   Generic(
        G_NBITS   : NATURAL := 8   -- Number of bits for the weights.
   );
   port (
       -- Port A inferface
       I_clk        : in  STD_LOGIC;
       I_rst        : in std_logic;
       I_vectors         : in T_STD_LOGIC_VECTOR8_ARRAY(9 downto 0);
       O_argmax        : out STD_LOGIC_VECTOR(3 downto 0)
   );
end ArgMax;
 
architecture maxw of ArgMax is
    signal TMP_argmax : T_STD_LOGIC_VECTOR4_ARRAY(7 downto 0);
    signal TMP_max : T_STD_LOGIC_VECTOR8_ARRAY(7 downto 0);
    signal argm: STD_LOGIC_VECTOR(3 downto 0);
    signal reg_argmax: STD_LOGIC_VECTOR(3 downto 0);
begin
    TMP_argmax(0) <= "0000" when (signed(I_vectors(0)) > signed(I_vectors(1))) else "0001";
    TMP_max(0) <= I_vectors(0) when (signed(I_vectors(0)) > signed(I_vectors(1))) else I_vectors(1);
    TMP_argmax(1) <= "0010" when (signed(I_vectors(2)) > signed(I_vectors(3))) else "0011";
    TMP_max(1) <= I_vectors(2) when (signed(I_vectors(2)) > signed(I_vectors(3))) else I_vectors(3);
    TMP_argmax(2) <= "0100" when (signed(I_vectors(4)) > signed(I_vectors(5))) else "0101";
    TMP_max(2) <= I_vectors(4) when (signed(I_vectors(4)) > signed(I_vectors(5))) else I_vectors(5);
    TMP_argmax(3) <= "0110" when (signed(I_vectors(6)) > signed(I_vectors(7))) else "0111";
    TMP_max(3) <= I_vectors(6) when (signed(I_vectors(6)) > signed(I_vectors(7))) else I_vectors(7);
    TMP_argmax(4) <= "1000" when (signed(I_vectors(8)) > signed(I_vectors(9))) else "1001";
    TMP_max(4) <= I_vectors(8) when (signed(I_vectors(8)) > signed(I_vectors(0))) else I_vectors(9);

    TMP_argmax(5) <= TMP_argmax(0) when (signed(TMP_max(0)) > signed(TMP_max(1))) else TMP_argmax(1);
    TMP_max(5) <= TMP_max(0) when (signed(TMP_max(0)) > signed(TMP_max(1))) else TMP_max(1);
    TMP_argmax(6) <= TMP_argmax(2) when (signed(TMP_max(2)) > signed(TMP_max(3))) else TMP_argmax(3);
    TMP_max(6) <= TMP_max(2) when  (signed(TMP_max(2)) > signed(TMP_max(3))) else TMP_max(3);
    TMP_argmax(7) <= TMP_argmax(4) when (signed(TMP_max(4)) > signed(TMP_max(6))) else TMP_argmax(6);
    TMP_max(7) <= TMP_max(4) when (signed(TMP_max(4)) > signed(TMP_max(6))) else TMP_max(6);
    argm <= TMP_argmax(5) when (signed(TMP_max(5)) > signed(TMP_max(7))) else TMP_argmax(7);

    
    process(I_clk, I_rst)
    begin
        if I_rst='1' then
            reg_argmax <= (others=>'0');
        elsif rising_edge(I_clk) then
            reg_argmax <= argm;
        end if;
    end process;

    O_argmax <= reg_argmax;

end maxw;