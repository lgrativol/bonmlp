----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.02.2019 19:41:36
-- Design Name: 
-- Module Name: tb_boneuron - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.utils_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_boneuron is
--  Port ( );
end tb_boneuron;

architecture Behavioral of tb_boneuron is

component boneuron is
    generic(
        G_FILEPATH      : STRING:="poids.txt";    
        --
        G_PIXEL_NBITS   : integer := 8; -- fix 0,8
        --
        G_NUMBER_PIXELS : integer := 784;
        G_OUTPUT_SIZE   : integer := 10;
        G_OUTPUT_PRECISION     : integer := 2;
        --
        G_WEIGHTS_NBITS : integer := 5;
        G_WEIGHTS_FRAC : integer := 4;
        --
        G_BIAS_NBITS    : integer := 5;
        G_BIAS_FRAC     : integer := 2;
        G_CTE_BIAS      : integer := 0
    );
    port(
        ------------------CLOCK RESET------------------
        I_clk           : in std_logic;
        I_arst          : in std_logic;
        ------------------------------------------------
        ------------------DATA--------------------------
        I_pixel         : in std_logic_vector(G_PIXEL_NBITS-1 downto 0);
        O_result        : out std_logic_vector (G_OUTPUT_SIZE-1 downto 0);
        I_mem_addr        : in std_logic_vector(log2(G_NUMBER_PIXELS)-1 downto 0);
        ------------------------------------------------
        -----------CONTROL------------------------------
        I_valid         : in std_logic;
        I_enable_output    : in std_logic
        ------------------------------------------------
    );
    
end component;

signal rst, valid, endInput : std_logic;
signal clk : std_logic:='0';
signal pixel:    std_logic_vector(7 downto 0);
signal result   : std_logic_vector(9 downto 0);
signal mem_addr : std_logic_vector( 9 downto 0);



begin

NEURON : boneuron 
    generic map(
      G_FILEPATH         =>"poids.txt",   
      --                 
      G_PIXEL_NBITS      => 8, -- fix 0,8
      --                 
      G_NUMBER_PIXELS    => 784,
      G_OUTPUT_SIZE      => 10,
      G_OUTPUT_PRECISION => 2,
      --                 
      G_WEIGHTS_NBITS    => 5,
      G_WEIGHTS_FRAC     => 4,
      --                 
      G_BIAS_NBITS       => 5,
      G_BIAS_FRAC        => 6,
      G_CTE_BIAS         => 13
    )      
    port map
    (
     I_clk          =>clk,     
     I_arst         =>rst,     
     -----------
     -----------
     I_pixel         =>pixel,      
     O_result        =>result,      
     I_mem_addr      =>mem_addr,     
     -----------
     -----------
     I_valid         =>valid,      
     I_enable_output =>endInput                     
    );
    
 --clock
 clk<= not clk after 5ns; -- period= 10ns
 
 -- stim
 
 stim: process
 begin
    
    rst<='1';
    wait for 20ns;
    rst<='0';
    valid<='0';
    endInput<='0';
    pixel<=(others=>'0');
    mem_addr<=(others=>'0');
    
    wait until rising_edge(clk); 
    wait for 10ns;
    
    for i in 0 to 783 loop
        mem_addr<=std_logic_vector(to_unsigned(i,10));
        pixel<= std_logic_vector(to_unsigned(i,8));
        valid<='1';
        wait for 10ns;
    end loop;
    
    endInput<='1';
    valid<='0';
    wait for 10ns;
    wait until rising_edge(clk);
    endInput<='0';
        
    wait;
 end process;   
    
    
    
    

end Behavioral;
