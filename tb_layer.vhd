----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 10.02.2019 21:18:33
-- Design Name: 
-- Module Name: tb_layer - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity tb_layer is
--  Port ( );
end tb_layer;

architecture Behavioral of tb_layer is

component layer is
  Generic(
    G_FILEPATH      : STRING:="poids.txt";    
    --
    G_PIXEL_NBITS       : integer := 8; -- fix 0,8
    G_HIDDEN_NEURONS   : integer := 40;
    --
    G_NUMBER_PIXELS : integer := 784;
    G_OUTPUT_SIZE   : integer := 10;
    G_OUTPUT_PRECISION     : integer := 2;
    --
    G_WEIGHTS_NBITS : integer := 5;
    G_WEIGHTS_FRAC : integer := 4;
    --
    G_BIAS_NBITS    : integer := 5;
    G_BIAS_FRAC     : integer := 2;
    G_CTE_BIAS      : integer := 0 -- TODO: FILE FOR BIAS
);
  Port ( 
    ---------------------------Clock resets-----------------------------
  I_clk                    : in std_logic;
  I_reset                  : in std_logic;
  -------------------------------------------------------------------
  I_ackInput               : in std_logic;
  I_nextLayerReady        : in std_logic;
  I_pixel                  : in std_logic_vector(G_PIXEL_NBITS-1 downto 0);
  O_result                 : out std_logic_vector(G_OUTPUT_SIZE-1 downto 0);
  O_layerReady            : out std_logic;
  O_requestInput           : out std_logic;
  -------------------------------------------------------------------
  I_layerRequestingInput   : in std_logic;
  O_layerAckingInput       : out std_logic
  ------------------------------------------------------------------

  );
end component;

signal rst, valid,nextReady,ready,requestInput,requestingInput,ackingInput : std_logic;
signal clk : std_logic:='0';
signal pixel:    std_logic_vector(7 downto 0);
signal result   : std_logic_vector(9 downto 0);

begin

layer_inst: layer
  Generic map(
    G_FILEPATH          =>"poids.txt",    
    --                  
    G_PIXEL_NBITS       => 8, -- fix 0,8
    G_HIDDEN_NEURONS    => 5,
    --                  
    G_NUMBER_PIXELS     => 784,
    G_OUTPUT_SIZE       => 10,
    G_OUTPUT_PRECISION  => 2,
    --                  
    G_WEIGHTS_NBITS     => 5,
    G_WEIGHTS_FRAC      => 4,
    --                  
    G_BIAS_NBITS        => 5,
    G_BIAS_FRAC         => 2,
    G_CTE_BIAS          => 0 -- TODO: FILE FOR BIAS
)
  Port map( 
    I_clk               =>clk,
    I_reset             =>rst,
    I_ackInput          => valid,
    I_nextLayerReady   => nextReady,
    I_pixel             => pixel,
    O_result            => result,
    O_layerReady       => ready,
    O_requestInput      => requestInput,
    I_layerRequestingInput => requestingInput,
    O_layerAckingInput     => ackingInput
  );

 --clock
 clk<= not clk after 5ns; -- period= 10ns
 requestingInput<='1';
 nextReady<='0';
 -- stim
 
 stim: process
 begin
    
    rst<='1';
    wait for 20ns;
    rst<='0';
    valid<='0';

    pixel<=(others=>'0');
    
    wait until rising_edge(clk); 
    wait for 10ns;
    
    for i in 0 to 783 loop
        pixel<= std_logic_vector(to_unsigned(i,8));
        valid<='1';
        wait for 10ns;
    end loop;
    valid<='0';
    wait for 10ns;
    wait until rising_edge(clk);
        
    wait;
 end process;   
    



end Behavioral;
