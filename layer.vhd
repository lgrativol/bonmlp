
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use work.utils_pkg.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity layer is
  Generic(
    G_LAYER_NUMBER          : STRING:="1";    
    -----------------------------------------------
    G_PIXEL_NBITS       : integer := 8; -- fix 0,8
    G_HIDDEN_NEURONS    : integer := 40;
    -----------------------------------------------
    G_NUMBER_PIXELS     : integer := 784;
    G_OUTPUT_SIZE       : integer := 8;
    G_OUTPUT_PRECISION  : integer := 2
    -----------------------------------------------
);
  Port ( 
    ---------------------------Clock resets-----------------------------
    I_clk                    : in std_logic;
    I_reset                  : in std_logic;
    -------------------------------------------------------------------
    I_ackInput               : in std_logic;
    O_layerIdle             : out std_logic;
    I_nextLayerReady        : in std_logic;
    I_pixel                  : in std_logic_vector(G_PIXEL_NBITS-1 downto 0);
    O_result                 : out std_logic_vector(G_OUTPUT_SIZE-1 downto 0);
    O_layerReady            : out std_logic;
    O_requestInput           : out std_logic;
    -------------------------------------------------------------------
    I_layerRequestingInput   : in std_logic;
    O_layerAckingInput       : out std_logic
    ------------------------------------------------------------------

  );
end layer;

architecture Behavioral of layer is

component control_unit is
    Port (
        ------ Clock ,input active-low reset; output active high reset --------
        I_clk           : in std_logic;
        I_reset         : in std_logic;
        -----------------------------------------------------------------------
        
        -------- User signals-------------------------------------------------- 
        I_ackInput         : in std_logic;
        I_nextReady     : in std_logic;
        I_endInput      : in std_logic;
        O_ready         : out std_logic;
        O_layerIdle    : out std_logic;
        O_requestInput  : out std_logic
        -----------------------------------------------------------------------

     );
end component;

component first_boneuron is
    generic(
        G_FILEPATH          : STRING:="poids.txt";    
        --
        G_PIXEL_NBITS       : integer := 8; -- fix 0,8
        --
        G_NUMBER_PIXELS     : integer := 784;
        G_OUTPUT_SIZE       : integer := 8;
        G_OUTPUT_PRECISION  : integer := 2;
        --
        G_WEIGHTS_NBITS     : integer := 5;
        G_WEIGHTS_FRAC      : integer := 4;
        --
        G_BIAS_NBITS        : integer := 5;
        G_BIAS_FRAC         : integer := 2;
        G_CTE_BIAS          : integer := 0
    );
    port(
        ------------------CLOCK RESET------------------
        I_clk               : in std_logic;
        I_arst              : in std_logic;
        I_rst_acc           : in std_logic;
        ------------------------------------------------
        ------------------DATA--------------------------
        I_pixel             : in std_logic_vector(G_PIXEL_NBITS-1 downto 0);
        O_result            : out std_logic_vector (G_OUTPUT_SIZE-1 downto 0);
        I_mem_addr          : in std_logic_vector(log2(G_NUMBER_PIXELS)-1 downto 0);
        ------------------------------------------------
        -----------CONTROL------------------------------
        I_valid             : in std_logic;
        I_enable_output     : in std_logic
        ------------------------------------------------
    );
    
end component;

----------------------------- Signals --------------------------------------

-----------------------------Constants-------------------------------------
constant G_ADDR_WIDTH : integer         := log2(G_NUMBER_PIXELS); -- declaration
constant G_BIAS_VALUES : T_INTEGER_ARRAY := read_data("./const/biasL" & G_LAYER_NUMBER, G_HIDDEN_NEURONS); -- bias values
constant G_WEIGHTS_NBITS : T_INTEGER_ARRAY := read_data("./const/weightL" & G_LAYER_NUMBER & "_NBits", G_HIDDEN_NEURONS); -- bias values
constant G_WEIGHTS_FRAC : T_INTEGER_ARRAY := read_data("./const/weightL" & G_LAYER_NUMBER & "_quantum", G_HIDDEN_NEURONS); -- bias values
constant G_BIAS_NBITS : T_INTEGER_ARRAY := read_data("./const/biasL" & G_LAYER_NUMBER & "_NBits", G_HIDDEN_NEURONS); -- bias values
constant G_BIAS_FRAC : T_INTEGER_ARRAY := read_data("./const/biasL" & G_LAYER_NUMBER & "_quantum", G_HIDDEN_NEURONS); -- bias values

--------------------------Memory signals-----------------------------------
signal mem_addr                         : unsigned(G_ADDR_WIDTH-1 downto 0);
signal mem_en                           : std_logic;
signal reset_mem_addr                   : std_logic;
-------------------------Control signals-----------------------------------
signal ready,ready_reg                   : std_logic;
signal tmp_endInput,endInput            : std_logic;
signal flag_endInput,flag_endInput_reg  : std_logic:='0';
signal output_counter                   : unsigned(log2(G_HIDDEN_NEURONS)-1 downto 0);
signal ackingInput                      : std_logic;
signal result                           : std_logic_vector(G_OUTPUT_SIZE-1 downto 0):=(others=>'0');
----------------------------------------------------------------------------
type t_hidden_result  is array(0 to G_HIDDEN_NEURONS) of std_logic_vector(G_OUTPUT_SIZE-1 downto 0);
signal hidden_result                    : t_hidden_result;
signal requestInput         :std_logic;
signal layerIdle      : std_logic;

signal ext_pixel        :std_logic_vector(G_PIXEL_NBITS downto 0);

begin

--    reg_layer_idle: process(I_clk,I_reset)
--    begin
--        if I_reset ='1' then
--            layer_idle_reg<='1';
--        elsif rising_edge(I_clk) then
--            if I_ackInput = '1' then
--                layer_idle_reg<='0';
--            elsif requestInput='0' then
--                layer_idle_reg<='1';
--            else
--                layer_idle_reg<='1';
--            end if;
--        end if;
--    end process;

--------------------------------------------------------------------------
    reset_mem_addr<= '1' when I_reset='1' or ready='1' else '0';
    mem_en<= I_ackInput;
    
    mem_addr_counter_proc: process(I_clk,reset_mem_addr)
    begin
        if reset_mem_addr = '1' then
            mem_addr<= (others=>'0');
            tmp_endInput<='0';
        elsif rising_edge(I_clk) then
            if mem_en ='1' then
                if to_integer(mem_addr) >= G_NUMBER_PIXELS-1 then
                    tmp_endInput<= '1';
                    mem_addr<= mem_addr;
                else
                    tmp_endInput<= '0';
                    mem_addr<= mem_addr +1;
                end if;
             end if;                      
        end if;
    end process;
--------------------------------------------------------------------------
--------------------------------------------------------------------------

    reg_endInput_proc: process(I_clk, I_reset)
    begin
        if I_reset ='1' then
            endInput<='0';
        elsif rising_edge(I_clk) then
            endInput<=tmp_endInput;
        end if; 
    
    end process;    
--------------------------------------------------------------------------
--------------------------------------------------------------------------
   
    reg_flag_endInput_proc: process(I_clk, I_reset)
    begin
        if I_reset ='1' then
            flag_endInput_reg<='0';
        elsif rising_edge(I_clk) then
            flag_endInput_reg<=flag_endInput;     
        end if; 
    end process;
    
    flag_endInput <= '1' when endInput='1' else '0' when output_counter=G_HIDDEN_NEURONS; -- TODO: revisar o flag low
--------------------------------------------------------------------------
--------------------------------------------------------------------------
    control_inst: control_unit
    Port Map (
        ------ Clock ,input reset; output active high reset --------
        I_clk        =>I_clk,
        I_reset      =>I_reset,
        -----------------------------------------------------------------------
        -------- User signals-------------------------------------------------- 
        I_ackInput         =>I_ackInput,
        I_nextReady        =>I_nextLayerReady,
        I_endInput         => endInput,
        O_ready            =>ready,
        O_layerIdle       =>layerIdle,
        O_requestInput     =>requestInput
       -----------------------------------------------------------------------
);
--------------------------------------------------------------------------
--------------------------------------------------------------------------

   GEN_BONEURON: 
   for I in 0 to (G_HIDDEN_NEURONS-1) generate
      NEURON : first_boneuron 
      generic map(
         -- G_FILEPATH         =>G_FILEPATH & integer'image(I) & ".lut" ,   
          G_FILEPATH         => "./const/weightL" & G_LAYER_NUMBER & "/weight" & integer'image(I) & ".lut",   
          ---------------------------------------------------                 
          G_PIXEL_NBITS      => G_PIXEL_NBITS , -- fix 0,8
          ---------------------------------------------------                 
          G_NUMBER_PIXELS    => G_NUMBER_PIXELS,
          G_OUTPUT_SIZE      => G_OUTPUT_SIZE,
          G_OUTPUT_PRECISION => G_OUTPUT_PRECISION,
          ---------------------------------------------------                 
          G_WEIGHTS_NBITS    => G_WEIGHTS_NBITS(I)+3,
          G_WEIGHTS_FRAC     => G_WEIGHTS_FRAC(I),
          ---------------------------------------------------                 
          G_BIAS_NBITS       => G_BIAS_NBITS(I)+3,
          G_BIAS_FRAC        => G_BIAS_FRAC(I),
          G_CTE_BIAS         => G_BIAS_VALUES(I)
      )      
      port map
      (
         I_clk              =>I_clk,     
         I_arst             =>I_reset,     
         I_rst_acc          => reset_mem_addr,
         -----------
         I_pixel            =>I_pixel,      
         O_result           =>hidden_result(I),      
         I_mem_addr         =>std_logic_vector(mem_addr),     
         -----------
         I_valid            =>I_ackInput,      
         I_enable_output    =>endInput                     
      );
   end generate GEN_BONEURON;
   
   
   output_proc: process(I_clk,I_reset)
   begin
        if I_reset='1' then  -- TODO, revisar reset do contador da saida
            output_counter<= (others=>'0');
        elsif rising_edge(I_clk) then
            if I_layerRequestingInput='1' and flag_endInput_reg='1' and output_counter<=G_HIDDEN_NEURONS  then
                ackingInput<='1';
                result<=hidden_result(to_integer(output_counter)); 
                --result<=std_logic_vector(unsigned(hidden_result(to_integer(output_counter)))+unsigned(output_counter)); 
                output_counter<= output_counter+1;
            else
                ackingInput<='0';
                output_counter<= (others=>'0');
            end if;
        end if;
   end process;
   
   --------------------------------------------------------------------------------
   --------------------------------------------------------------------------------
   reg_ready_proc: process(I_clk, I_reset)
   begin
       if I_reset ='1' then
           ready_reg<='0';
       elsif rising_edge(I_clk) then
           ready_reg<=ready;     
       end if; 
   end process;
   
   ------------------------------ OUTPUT -------------------------------
   O_result<= result;
   O_layerAckingInput <= ackingInput;
   O_layerReady<=ready_reg;
   O_requestInput<=requestInput;
   O_layerIdle<=layerIdle;
  
end Behavioral;
