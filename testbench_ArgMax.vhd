library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
USE work.utils_pkg.ALL;

-- FPGA projects using Verilog code VHDL code
-- fpga4student.com: FPGA projects, Verilog projects, VHDL projects
-- VHDL project: VHDL code for counters with testbench  
-- VHDL project: Testbench VHDL code for up counter
entity tb_argmax is
end tb_argmax;

architecture Behavioral of tb_argmax is

component ArgMax 
port (
	-- Port A inferface
	I_clk        : in  STD_LOGIC;
	I_rst: in std_logic;
	I_vectors         : in T_STD_LOGIC_VECTOR8_ARRAY(9 downto 0);
	O_argmax        : out STD_LOGIC_VECTOR(3 downto 0)
);
end component;
signal vectors: T_STD_LOGIC_VECTOR8_ARRAY(9 downto 0);
signal argm:  STD_LOGIC_VECTOR(3 downto 0);
signal clk: STD_LOGIC;
signal rst: STD_LOGIC;
begin
dut: ArgMax port map (I_clk => clk, I_rst => rst, I_vectors =>vectors, O_argmax => argm);
vectors(0) <= "00000001";
vectors(1) <= "00001001";
vectors(2) <= "00000001";
vectors(3) <= "01000000";
vectors(4) <= "00000001";
vectors(5) <= "00010001";
vectors(6) <= "00000001";
vectors(7) <= "00001101";
vectors(8) <= "01001001";
vectors(9) <= "00001001";
   -- Clock process definitions
clock_process :process
begin
     clk <= '0';
     wait for 10 ns;
     clk <= '1';
     wait for 10 ns;
end process;

rst <= '1', '0' after 50ns;
end Behavioral;