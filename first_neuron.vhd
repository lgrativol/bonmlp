library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.utils_pkg.ALL;
--use work.ram_pkg.ALL;

entity first_boneuron is
    generic(
        -----------------------------------------------------------------------------------
        G_FILEPATH          : STRING  :="C:\Users\lucas\Desktop\perso\perso\project_1\poids.txt"; -- Weigths path    
        -----------------------------------------------------------------------------------
        G_PIXEL_NBITS       : integer := 8;  -- Input size in bits
        -----------------------------------------------------------------------------------
        G_NUMBER_PIXELS     : integer := 784; -- Number of inputs 
        G_OUTPUT_SIZE       : integer := 10; -- Output size in bits
        G_OUTPUT_PRECISION  : integer := 2; -- Output precision (?????)
        -----------------------------------------------------------------------------------
        G_WEIGHTS_NBITS     : integer := 5; -- Weights size in bits
        G_WEIGHTS_FRAC      : integer := 4; -- Weights frac position
        -----------------------------------------------------------------------------------
        G_BIAS_NBITS        : integer := 5; -- Bias size in bits
        G_BIAS_FRAC         : integer :=5; -- Bias frac position
        G_CTE_BIAS          : integer := 13 -- Bias 
        -----------------------------------------------------------------------------------
    );
    port(
        -------------------------CLOCK, ARESET-------------------------
        I_clk           : in std_logic; -- clk
        I_arst          : in std_logic; -- reset async, active HIGH or low?
        I_rst_acc       : in std_logic;
        ---------------------------------------------------------------
        -------------------------DATA----------------------------------
        I_pixel         : in std_logic_vector(G_PIXEL_NBITS-1 downto 0); -- Input with G_PIXEL_NBITS bits, not necessary pixels
        O_result        : out std_logic_vector (G_OUTPUT_SIZE-1 downto 0); -- Result after G_NUMBER_PIXELS
        I_mem_addr      : in std_logic_vector(log2(G_NUMBER_PIXELS)-1 downto 0); -- Address to acess the wights
        ---------------------------------------------------------------
        --------------------------CONTROL------------------------------
        I_valid         : in std_logic;  -- enables input
        I_enable_output : in std_logic -- enables output register
        ---------------------------------------------------------------
    );
    
end first_boneuron;

architecture arch_boneuron of first_boneuron is

    component generic_LUT_unit is
        Generic(
            G_FILEPATH     : STRING;                    -- File path where the value to store are located.
            G_DEPTH_LUT    : NATURAL;                   -- Depth of the LUT.
            G_NBIT_LUT     : NATURAL;                   -- Wordsize of the LUT.
            G_STYLE        : STRING := "distributed";   -- Synthesis option: "distributed" uses slices, "block" uses BRAM.
            G_PIPELINE_REG : BOOLEAN := FALSE           -- Option to add a pipeline register (2 cycles to read!)
        );
        Port( 
            I_clk        : in STD_LOGIC;                                         -- Typical system clock.
            I_sel_sample : in STD_LOGIC_VECTOR( log2(G_DEPTH_LUT) - 1 downto 0); -- Address of the LUT.
            O_LUT_value  : out STD_LOGIC_VECTOR( G_NBIT_LUT-1 downto 0 )         -- Output data of the LUT.
        );
    end component;

-----------------------------Constants--------------------------------------
constant G_ADDR_WIDTH : integer    := log2(G_NUMBER_PIXELS); -- function log2 to find array size
constant G_ACC_REG_WIDTH : integer := 0; -- TODO
-----------------------------------------------------------------------------
------------------------General signals and registers------------------------
signal pixel_reg                : std_logic_vector(G_PIXEL_NBITS-1 downto 0);
--signal mult_temp                : unsigned(G_PIXEL_NBITS+G_WEIGHTS_NBITS-1 downto 0 );
signal pixel_mult_weight        : std_logic_vector(G_PIXEL_NBITS+G_WEIGHTS_NBITS-1 downto 0 ); -- "wire" for mult. pixel*weigth
--signal pixel_mult_weight_pos    : std_logic_vector(G_PIXEL_NBITS+G_WEIGHTS_NBITS-1 downto 0 );
signal pixel_mult_weight_neg    : std_logic_vector(G_PIXEL_NBITS+G_WEIGHTS_NBITS downto 0 );
signal weigth_in_line           : std_logic_vector(G_WEIGHTS_NBITS-1 downto 0); -- lut_weigth(pixel)
signal weigth_in_line_reg           : std_logic_vector(G_WEIGHTS_NBITS-1 downto 0); -- lut_weigth(pixel)
signal output_reg               : std_logic_vector(G_OUTPUT_SIZE-1 downto 0); -- output register
signal acc_reg                  : std_logic_vector(G_PIXEL_NBITS+G_WEIGHTS_NBITS-1 downto 0 );-- register to accumulate the operation: pixel * weigth,

signal rst : std_logic;

 
----------------------------------------------------------------------------
--signal out_en                   : std_logic;
--Memory signals
--signal mem_addr             : unsigned(G_ADDR_WIDTH-1 downto 0);
--signal mem_en               : std_logic:='0';
---------------------------------------------------------------------------
begin

    rst<= I_rst_acc or I_arst;
    -- Input register
    pixel_reg_proc : process (I_clk,I_arst) is
    begin
        if I_arst= '1' then
            pixel_reg <= (others =>'0');
        elsif rising_edge(I_clk) then
            if I_valid ='1' then
                pixel_reg<=  I_pixel;
            end if;
        end if;
    end process;
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------   
--    mem_addr_counter_proc: process(I_clk,I_arst)
--    begin
--        if I_arst = '1' then
--            mem_addr<= (others=>'0');
--        elsif rising_edge(I_clk) then
--            if I_valid ='1' then
--                if to_integer(mem_addr) >= G_NUMBER_PIXELS then
--                    mem_en<='0';
--                    mem_addr<= mem_addr;
--                else
--                    mem_en<='1';
--                    mem_addr<= mem_addr +1;
--                end if;
--            end if;
--        end if;
--    end process;
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
    -- Memory instantiation (here using LUTs)
    mem_rom_inst: generic_LUT_unit
	Generic map (
        G_FILEPATH     => G_FILEPATH,       -- File path where the value to store are located.
        G_DEPTH_LUT    => G_NUMBER_PIXELS,  -- Depth of the LUT.
        G_NBIT_LUT     => G_WEIGHTS_NBITS,  -- Wordsize of the LUT.
        G_STYLE        => "distributed",    -- Synthesis option: "distributed" uses slices, "block" uses BRAM.
        G_PIPELINE_REG => FALSE             -- Option to add a pipeline register (2 cycles to read!)    
    )
    Port map (
        I_clk          => I_clk,            -- Typical system clock.
        I_sel_sample   => I_mem_addr,       -- Address of the LUT.
        O_LUT_value    => weigth_in_line    -- Output data of the LUT.
    );
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
    --Multiplication process
    --pixel_mult_weight <= std_logic_vector(unsigned(pixel_reg) * unsigned(weight_in_line));
    
    reg_weigth_in_line: process(I_clk,I_arst)
    begin
        if I_arst='1' then
            weigth_in_line_reg<=(others=>'0');
        elsif rising_edge(I_clk) then
            weigth_in_line_reg<= weigth_in_line;
        end if;
    end process;
    
    
    pixel_mult_weight_neg<= std_logic_vector(signed('0'&pixel_reg) * signed(weigth_in_line_reg));
    --pixel_mult_weight_neg(pixel_mult_weight_neg'length-2)<= pixel_mult_weight_neg(pixel_mult_weight_neg'length-1); 
    pixel_mult_weight <= pixel_mult_weight_neg(pixel_mult_weight_neg'length-2 downto 0);
   
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
    -- Register to accumulate the multiplication, it starts with the bias
    acc_proc : process (I_clk,rst) is
    begin
        if rst='1' then
            acc_reg<= std_logic_vector(SHIFT_LEFT(resize(to_signed(G_CTE_BIAS,G_BIAS_NBITS),G_PIXEL_NBITS+G_WEIGHTS_NBITS),G_WEIGHTS_FRAC+G_PIXEL_NBITS-G_BIAS_FRAC));
        elsif rising_edge(I_clk) then
            if I_valid='1' then
                acc_reg <= std_logic_vector(signed(acc_reg) + signed(pixel_mult_weight)); 
            end if;
        end if;
    end process;
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
    -- Output register
    output_reg_inst: process(I_clk,I_arst)
    begin
        if I_arst='1' then
            output_reg<=(others=>'0');
        elsif rising_edge(I_clk) then
            if I_enable_output ='1' then
                output_reg<= acc_reg(acc_reg'length-1 downto acc_reg'length-G_OUTPUT_SIZE); -- TODO: output format parameter
            end if;
        end if;
    end process;
---------------------------------------------------------------------------------------
---------------------------------------------------------------------------------------
   
    ------------------OUTPUT---------------------------------
    O_result<= (others=>'0') when output_reg(output_reg'length-1)='1' else output_reg;
    ---------------------------------------------------------    
    
end architecture;


        
