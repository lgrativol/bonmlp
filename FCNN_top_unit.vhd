library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use work.utils_pkg.ALL;

-- Top level of the Fully Connected Neural Network (FCNN) unit.
entity FCNN_top_unit is
	 Generic(
        G_NBITS_PIXEL     : NATURAL := 8;   -- Number of bits for the input pixels.
        G_NUMBER_PIXELS   : NATURAL := 784; -- Number of input pixels in an image.
        G_NUMBER_CLASSES  : NATURAL := 10;  -- Number of classes (= Depth of the output layer)
        G_NBITS_WEIGHTS   : NATURAL := 5;   -- Number of bits for the weights.
        G_LUT_FOLDER      : STRING  := "../LUT_files"   -- Path of the folder containing the LUTs.
	 );
    Port ( 
    
         -------------------------- CLOCK AND RESET -------------------------
         I_clk        : in  STD_LOGIC;   -- System clock.
	     I_aync_rst   : in  STD_LOGIC;   -- Asnchronous, active-low reset.
         --------------------------------------------------------------------
           
         -------------------------- CONTROL SIGNALS -------------------------   
         O_requestPixel : out STD_LOGIC; -- Request a new pixel.
         I_ackPixel     : in  STD_LOGIC; -- Acknowledge the request and send a valid "I_pixel" signal.
         O_classifValid : out STD_LOGIC; -- The "O_classif" output provides a valid result.
         O_readyClassif : out STD_LOGIC; -- Inform that the processor is ready to process a new classif.
         --------------------------------------------------------------------
         
         -------------------------- INPUT/OUPUT DATA ------------------------ 
         I_pixel      : in  STD_LOGIC_VECTOR (G_NBITS_PIXEL-1 downto 0); -- 
         O_classif    : out STD_LOGIC_VECTOR (log2(G_NUMBER_CLASSES)-1 downto 0)
         -------------------------- INPUT/OUPUT DATA ------------------------ 
         
         );
end FCNN_top_unit;

architecture Behavioral of FCNN_top_unit is

component layer is
  Generic(
    G_LAYER_NUMBER      : STRING:="1";    
    -----------------------------------------------
    G_PIXEL_NBITS       : integer := 8; -- fix 0,8
    G_HIDDEN_NEURONS    : integer := 40;
    -----------------------------------------------
    G_NUMBER_PIXELS     : integer := 784;
    G_OUTPUT_SIZE       : integer := 10;
    G_OUTPUT_PRECISION  : integer := 2
    -----------------------------------------------

);
  Port ( 
    ---------------------------Clock resets-----------------------------
    I_clk                    : in std_logic;
    I_reset                  : in std_logic;
    -------------------------------------------------------------------
    I_ackInput               : in std_logic;
    O_layerIdle             : out std_logic;
    I_nextLayerReady        : in std_logic;
    I_pixel                  : in std_logic_vector(G_PIXEL_NBITS-1 downto 0);
    O_result                 : out std_logic_vector(G_OUTPUT_SIZE-1 downto 0);
    O_layerReady            : out std_logic;
    O_requestInput           : out std_logic;
    -------------------------------------------------------------------
    I_layerRequestingInput   : in std_logic;
    O_layerAckingInput       : out std_logic
    ------------------------------------------------------------------

  );
end component;

component output_layer is
  Generic(
    G_LAYER_NUMBER          : STRING:="1"; 
    -----------------------------------------------
    G_PIXEL_NBITS       : integer := 8; -- fix 0,8
    G_HIDDEN_NEURONS    : integer := 10;
    -----------------------------------------------
    G_NUMBER_PIXELS     : integer := 784;
    G_OUTPUT_SIZE       : integer := 8;
    G_OUTPUT_PRECISION  : integer := 2;
    -----------------------------------------------
    G_WEIGHTS_NBITS     : integer := 5;
    G_WEIGHTS_FRAC      : integer := 4;
    -----------------------------------------------
    G_BIAS_NBITS        : integer := 5;
    G_BIAS_FRAC         : integer := 2;
    G_CTE_BIAS          : integer := 0 -- TODO: FILE FOR BIAS
    -----------------------------------------------

);
  Port ( 
    ---------------------------Clock resets-----------------------------
    I_clk                    : in std_logic;
    I_reset                  : in std_logic;
    -------------------------------------------------------------------
    I_ackInput               : in std_logic;
    O_layerIdle             : out std_logic;
    I_nextLayerReady        : in std_logic;
    I_pixel                  : in std_logic_vector(G_PIXEL_NBITS-1 downto 0);
    O_result                 : out T_STD_LOGIC_VECTOR8_ARRAY(9 downto 0);
    O_layerReady            : out std_logic;
    O_requestInput           : out std_logic;
    -------------------------------------------------------------------
    I_layerRequestingInput   : in std_logic;
    O_layerAckingInput       : out std_logic
    ------------------------------------------------------------------

  );
end component;


component ArgMax is
   Generic(
        G_NBITS   : NATURAL := 8   -- Number of bits for the weights.
   );
   port (
       -- Port A inferface
       I_clk        : in  STD_LOGIC;
       I_rst: in std_logic;
       I_vectors         : in T_STD_LOGIC_VECTOR8_ARRAY(9 downto 0);
       O_argmax        : out STD_LOGIC_VECTOR(3 downto 0)
   );
end component;

	--------------------------------------------------
	--					CONSTANT DECLARATION		         --
	--------------------------------------------------
	-- Constant are defined here.
                ------- LAYER 1 ---------
    constant G_LAYER_1_INPUT_SIZE     : integer:=8;
    constant G_LAYER_1_NEURONS        : integer:=40;
    constant G_LAYER_1_INPUT_NUMBER   : integer:=784;
    constant G_LAYER_1_OUTPUT_SIZE    : integer:=10;
    constant G_LAYER_1_WEIGHTS_SIZE   : integer:=5;
    constant G_LAYER_1_WEIGHTS_FRAC   : integer:=4;
    constant G_LAYER_1_BIAS_SIZE      : integer:=5;
    constant G_LAYER_1_BIAS_FRAC      : integer:=4;

                ------- LAYER 2 ---------
    constant G_LAYER_2_INPUT_SIZE     : integer:=10;
    constant G_LAYER_2_NEURONS        : integer:=10;
    constant G_LAYER_2_INPUT_NUMBER   : integer:=40;
    constant G_LAYER_2_OUTPUT_SIZE    : integer:=8;
    constant G_LAYER_2_WEIGHTS_SIZE   : integer:=5;
    constant G_LAYER_2_WEIGHTS_FRAC   : integer:=4;
    constant G_LAYER_2_BIAS_SIZE      : integer:=5;
    constant G_LAYER_2_BIAS_FRAC      : integer:=4;

	
 	--------------------------------------------------
	--				  TYPE DECLARATION                  --
	--------------------------------------------------
	
   -- Types are defined here.
   
   --------------------------------------------------
	--				  SIGNAL DECLARATION                --
	--------------------------------------------------
    
                    ---- LAYER 1-----
    signal layer_1_result        : std_logic_vector(G_LAYER_1_OUTPUT_SIZE-1 downto 0);                 
    signal layer_1_requestInput,layer_1_ready,layer_1_layerRequestingInput,layer_1_layerAckingInput: std_logic;
    
                    ---- LAYER 2(output) -----
    signal layer_2_result        : T_STD_LOGIC_VECTOR8_ARRAY(9 downto 0);                 
    signal layer_2_requestInput,layer_2_ready,layer_2_layerRequestingInput,layer_2_layerAckingInput: std_logic;
    signal layer_2_idle: std_logic;
    signal layer_2_ready_cond: std_logic;
    
                   ---- LAYER 3(output) -----
    signal layer_3_ready    : std_logic;
    
    ------- Control signals
    signal readyClassif_reg :std_logic;
    

begin

------- Hidden Layer with 40 neurons---------------------------

  layer_1: layer
  Generic Map(
    G_LAYER_NUMBER      => "1",      
    --------------------=>-----------------
    G_PIXEL_NBITS       => G_LAYER_1_INPUT_SIZE, -- fix 0,8
    G_HIDDEN_NEURONS    => G_LAYER_1_NEURONS,
    --------------------=>---------------
    G_NUMBER_PIXELS     => G_LAYER_1_INPUT_NUMBER,
    G_OUTPUT_SIZE       => G_LAYER_1_OUTPUT_SIZE,
    G_OUTPUT_PRECISION  => 2
    -----------------------------------------------
)
  Port Map ( 
    ---------------------------Clock resets-----------------------------
    I_clk                    =>I_clk,
    I_reset                  =>I_aync_rst,
    -------------------------=>,
    I_ackInput               =>I_ackPixel,
    O_layerIdle              => open,
    I_nextLayerReady         =>layer_2_ready_cond,
    --I_nextLayerReady         =>'1',
    I_pixel                  =>I_pixel,
    O_result                 =>layer_1_result,
    O_layerReady             =>layer_1_ready,
    O_requestInput           => O_requestPixel,
    -------------------------=>,
    I_layerRequestingInput   =>layer_2_requestInput,
    O_layerAckingInput       =>layer_1_layerAckingInput
    ------------------------------------------------------------------
  );
  
    layer_2_ready_cond<= layer_2_ready or layer_2_idle;

  ---------------- Output layer (10 layers)--------------------------
    layer_output: output_layer
  Generic Map(
    G_LAYER_NUMBER          =>"2",    
    --------------------=>-----------------
    G_PIXEL_NBITS       => G_LAYER_2_INPUT_SIZE, -- fix 0,8
    G_HIDDEN_NEURONS    => G_LAYER_2_NEURONS,
    --------------------=>---------------
    G_NUMBER_PIXELS     => G_LAYER_2_INPUT_NUMBER,
    G_OUTPUT_SIZE       => G_LAYER_2_OUTPUT_SIZE,
    G_OUTPUT_PRECISION  => 2,
    --------------------=>---------------
    G_WEIGHTS_NBITS     => G_LAYER_2_WEIGHTS_SIZE,
    G_WEIGHTS_FRAC      => G_LAYER_2_WEIGHTS_FRAC,
    --------------------=>---------------
    G_BIAS_NBITS        => G_LAYER_2_BIAS_SIZE,
    G_BIAS_FRAC         => G_LAYER_2_BIAS_FRAC,
    G_CTE_BIAS          => 0 -- TODO: FILE FOR BIAS
    -----------------------------------------------
)
  Port Map ( 
    ---------------------------Clock resets-----------------------------
    I_clk                    =>I_clk,
    I_reset                  =>I_aync_rst,
    -------------------------=>,
    I_ackInput               =>layer_1_layerAckingInput,
    O_layerIdle              =>layer_2_idle,
    I_nextLayerReady         => '1',
    I_pixel                  =>layer_1_result,
    O_result                 =>layer_2_result, -- output is different
    O_layerReady             =>layer_2_ready,
    O_requestInput           => layer_2_requestInput,
    I_layerRequestingInput   =>'0',
    O_layerAckingInput       =>layer_2_layerAckingInput
    ------------------------------------------------------------------
  );
  
  argmax_inst: argmax 
     Generic map(
          G_NBITS   => 8   -- Number of bits for the weights.
     )
     port map(
         -- Port A inferface
         I_clk    =>I_clk,
         I_rst    => I_aync_rst,
         I_vectors =>layer_2_result,  
         O_argmax  =>O_classif
     );
     
     ready_reg: process(I_clk,I_aync_rst)
     begin
        if I_aync_rst ='1' then
            readyClassif_reg<='0';
        elsif rising_edge(I_clk) then
            readyClassif_reg<=layer_2_ready;
        end if;
     end process;
     
     O_classifValid<=readyClassif_reg;
     O_readyClassif<=layer_1_ready;
  
end Behavioral;
